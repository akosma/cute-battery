#!/usr/bin/env bash

# This script takes a number, either as argument or via stdin,
# representing the current charge of the battery. If the number is
# negative, it means that the battery is DISCHARGING; if the number is
# positive, it means that the battery is CHARGING, and then a small
# emoji is printed to the left of the indicator.

# Usage:
#
# $ echo "-33" | ./cutinate.sh
# ●●●○○○○○○○
#
# $ ./cutinate.sh 64
# ⚡︎●●●●●●○○○○
#
# $ ./cutinate.sh 65
# ⚡︎●●●●●●●○○○
#
# You can also specify the total number of icons (10 by default):
#
# $ NUM_ICONS=30 ./cutinate.sh 75
# ⚡︎●●●●●●●●●●●●●●●●●●●●●●●●○○○○○○

# The following table gives, for each range of battery power percentage,
# the number of icons that will be shown by the algorithm (when the
# variable NUM_ICONS is equal to 10):
# 0-4: 0
# 5-14: 1
# 15-24: 2
# 25-34: 3
# 35-44: 4
# 45-54: 5
# 55-64: 6
# 65-74: 7
# 75-84: 8
# 85-94: 9
# 95-100: 10

ICON_FULL=●
ICON_EMPTY=○
[ -z "$NUM_ICONS" ] &&
    NUM_ICONS=10

cutinate() {
    percentage=$1
    if [[ $percentage == -* ]]; then
        # If the parameter is NEGATIVE, then the battery is DISCHARGING
        percentage=$((-1 * percentage))
    else
        # If the parameter is POSITIVE, then the battery is CHARGING,
        # hence show a "bolt" sign to indicate charging status.
        printf "⚡︎"
    fi
    # Reduce the percentage to a number of icons, proportional to
    # the NUM_ICONS variable (increasing by 5 to round up)
    value=$(awk -v icons="$NUM_ICONS" -v value="$percentage" 'BEGIN {print int((value + 5) / 100 * icons)}')

    # Print the icons
    for i in $(seq "$NUM_ICONS"); do
        if [ "$i" -le "$value" ]; then
            printf "%s" $ICON_FULL
        else
            printf "%s" $ICON_EMPTY
        fi
    done
    echo
}

# Determine the operating system
OS="$(uname)"

# Function to handle input via stdin
handle_input() {
    if [ $# -gt 0 ]; then
        echo "$1"
    else
        case "$OS" in
        FreeBSD | Darwin)
            if [ -t 0 ]; then
                echo "No input provided"
            else
                while IFS= read -r line; do
                    echo "$line"
                done
            fi
            ;;
        Linux)
            if [ -p /dev/stdin ]; then
                while IFS= read -r line; do
                    echo "$line"
                done
            else
                echo "No input provided"
            fi
            ;;
        *)
            echo "Unsupported OS: $OS"
            ;;
        esac
    fi
}

# Execute the function with passed arguments
VALUE=$(handle_input "$@")

cutinate "$VALUE"
