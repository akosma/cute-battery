#!/usr/bin/env bash

linux_get_bat() {
    BATPATH=${BATPATH:-/sys/class/power_supply/BAT0}
    if [ -f "$BATPATH/energy_full" ]; then
        naming="energy"
    elif [ -f "$BATPATH/charge_full" ]; then
        naming="charge"
    fi
    BAT_STATUS=$BATPATH/status
    BAT_FULL=$BATPATH/${naming}_full
    BAT_NOW=$BATPATH/${naming}_now
    bs=$(cat "$BAT_STATUS")
    bf=$(cat "$BAT_FULL")
    bn=$(cat "$BAT_NOW")
    if [ "$bs" = "Discharging" ]; then
        BAT=$(echo "-100 * $bn / $bf" | bc)
    elif [ "$bs" = "Full" ]; then
        BAT=$(echo "-100" | bc)
    else
        BAT=$(echo "100 * $bn / $bf" | bc)
    fi
    echo "$BAT"
}

freebsd_get_bat() {
    VALUE=$(sysctl -n hw.acpi.battery.life) # Battery charge %
    STATE=$(sysctl -n hw.acpi.acline)       # 1 when plugged and charging, 0 when on battery
    if [ "$STATE" -eq 0 ]; then
        BAT=$(echo "-1 * $VALUE" | bc) # Make it compatible with "cutinate.sh"
    else
        BAT=$VALUE
    fi
    echo "$BAT"
}

darwin_get_bat() {
    ioreg -c AppleSmartBattery -w0 |
        grep -o '"[^"]*" = [^ ]*' |
        sed -e 's/= //g' -e 's/"//g' |
        sort |
        while read -r key value; do
            case $key in
            "MaxCapacity")
                export maxcap=$value
                ;;
            "CurrentCapacity")
                export curcap=$value
                ;;
            "PackReserve")
                export packres=$value
                ;;
            "IsCharging")
                export charging=$value
                ;;
            "FullyCharged")
                if [ "$value" = "Yes" ]; then
                    # The battery is fully charged!
                    echo "100"
                    break
                fi
                ;;
            esac
            if [[ -n "$maxcap" && -n $curcap && -n $packres && -n $charging ]]; then
                if [ "$charging" = "Yes" ]; then
                    echo "100 * ($curcap + $packres) / $maxcap" | bc
                    break
                else
                    echo "- 100 * ($curcap + $packres) / $maxcap" | bc
                    break
                fi
            fi
        done
}

battery_status() {
    case $(uname -s) in
    "Linux")
        linux_get_bat
        ;;
    "FreeBSD")
        freebsd_get_bat
        ;;
    "Darwin")
        darwin_get_bat
        ;;
    esac
}

BATTERY_STATUS=$(battery_status "$1")
[ -z "$BATTERY_STATUS" ] && exit

echo "$BATTERY_STATUS"
