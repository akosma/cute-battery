# Cute Battery Level Display

This set of scripts provide a nice display of the battery on Linux, FreeBSD, and macOS (Darwin).

- The `battery.sh` script returns the current level of the battery in percentage (0-100).
    - A negative value means that the battery is discharging.
    - A positive value means that the battery is charging.
- The `cutinate.sh` script takes a numerical value, either from `stdin` or as argument, and displays a series of bullets.
    - If the value passed to the script is positive, then a lightning bolt emoji is shown on the left.

Adapted from ["Showing your laptop battery status in tmux"](https://effectif.com/system-administration/battery-status-in-tmux).

## Install

Copy these scripts on your `$PATH` and make them executable.

## How to Use

```
$ battery.sh | cutinate.sh
⚡︎●●●○○○○○○○
```

You can also specify the maximum number of icons to display:

```
$ battery.sh | NUM_ICONS=50 cutinate.sh
⚡︎●●●●●●●●●●●●●●●●●●○○○○○○○○○○○○○○○○○○○○○○○○○○○○○○○○
```

The `cutinate.sh` script can be used with anything returning an integer between 0 and 100:

```
$ echo "32" | cutinate.sh
⚡︎●●●○○○○○○○
$ cutinate.sh 56
⚡︎●●●●●●○○○○
```

You can display this on [tmux](https://github.com/tmux/tmux/wiki) adding this line to your `.tmux.conf` file:

```
set -g status-right "| #(battery.sh | cutinate.sh) | %a %b %e, %H:%M "
```

